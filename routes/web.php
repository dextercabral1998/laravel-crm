<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->middleware('auth');


Auth::routes(['register' => false,
'reset' => false,
  'verify' => false, ]);

Route::group(['middleware' => 'auth'],function(){

    Route::get('/', 'HomeController@index');
    Route::get('home', 'HomeController@index');
    Route::get('admin','adminController@index');

    //company
    Route::get('createCompany','companyController@create');
    Route::get('companyList','companyController@index');
    Route::get('companyEdit/{id}','companyController@edit');
    Route::get('companyDelete/{id}','companyController@delete');
    Route::post('saveCompany','companyController@store');
    Route::post('updateCompany','companyController@update');


    //employee
    Route::get('createEmployee','employeeController@create');
    Route::get('viewEmployee/{companyId}','employeeController@viewEmployee');
    Route::get('employeeList','employeeController@index');
    Route::get('employeeEdit/{id}','employeeController@edit');
    Route::get('employeeDelete/{id}','employeeController@delete');
    Route::post('saveEmployee','employeeController@store');
    Route::post('updateEmployee','employeeController@update');


});
