<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class companyModel extends Model
{
    public $timestamps = false;
    protected $table = "company";
    protected $primaryKey = "company_id";
    protected $fillable = [
        'name','email','logo','website'
    ];
}
