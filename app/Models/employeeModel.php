<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class employeeModel extends Model
{
    public $timestamps = false;
    protected $table = "employee";
    protected $primaryKey = "emp_id";
    protected $fillable = [
        'firstName','lastName','email','phone','companyId'
    ];
}
