<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\companyModel as company;
use Session;

class companyController extends Controller
{
    //

    public function index()
    {
        $companies = company::paginate(10);
        return view('company.listCompany')->with(compact('companies'));
    }
    public function create()
    {
        return view('company.createCompany');
    }
    public function edit($id)
    {
        $company = company::where('company_id',$id)->first();
        return view('company.editCompany')->with(compact('company'));;
    }
    public function delete($id)
    {
        $company =  company::find($id);
        $company->delete();

        return "success";
    }
    public function update(Request $request)
    {
        $id = $request->id;
        $logoHolder = $request->logoHolder;
        $name = $request->name;
        $email = $request->email;
        $website = $request->website;

        if ($request->logo != "")
        {
            $validatedData = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|email',
                'website' => 'required',
                'logo' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
            ]);
            $image = $request->logo;
            $uploadedImage = $request->file('logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/company_logo');
            $uploadedImage->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;
            $logo = $imageName;
        }

        else{
            $validatedData = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|email',
                'website' => 'required',

            ]);
            $logo = $logoHolder;
        }

        company::where('company_id',$id)->update([
            'name' => $name,
            'email' => $email,
            'website' => $website,
            'logo' => $logo,
        ]);

        return redirect('companyList')->with('message','Success!');

    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:company|max:255',
            'email' => 'required|email|unique:company',
            'website' => 'required',
            'logo' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
        ]);


        $name = $request->name;
        $email = $request->email;
        $website = $request->website;
        $image = $request->logo;


        if (request()->hasFile('logo')){
            $uploadedImage = $request->file('logo');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/company_logo');
            $uploadedImage->move($destinationPath, $imageName);
            $image->imagePath = $destinationPath . $imageName;

            $company = new company;
            $company->name = $name;
            $company->email = $email;
            $company->website = $website;
            $company->logo = $imageName;
            $company->save();


            $this->sendEmail($company->toArray());
        }

        return redirect('createCompany')->with('message','Success!');
    }
    public function sendEmail($company)
    {
        Mail::send('email.sendCompany',$company,function($message){
            $message->to('dextercabral1998@gmail.com','Code Online')->subject('Company Created Subject');
        });
    }

}
