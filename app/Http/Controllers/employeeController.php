<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\companyModel as company, App\Models\employeeModel as employee;
use Session, DB;
class employeeController extends Controller
{
    public function index()
    {
        $employees = DB::table('employee as emp')
            ->join('company as comp', 'emp.companyID', '=', 'comp.company_id')
            ->select('emp.emp_id as id','emp.firstName','emp.lastName','emp.email','emp.phone', 'comp.name as company_name')
            ->paginate(10);
        // $employees = employee::paginate(10);
        return view('employee.employeeList')->with(compact('employees'));
    }
    public function create()
    {
        $company = company::select('company_id','name')->get();
        return view('employee.createEmployee')->with(compact('company'));
    }
    public function edit($id)
    {
        $company = company::select('company_id','name')->get();
        $employee = employee::where('emp_id',$id)->first();
        return view('employee.editEmployee')->with(compact('employee','company'));
    }
    public function delete($id)
    {
        $company =  employee::find($id);
        $company->delete();

        return "Success";
    }
    public function viewEmployee($companyID)
    {
        $employees = employee::where('companyID',$companyID)->get();
        return view('employee.employeeTableList')->with(compact('employees'));
    }
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required',
            'company' => 'required',
            'id' => 'required',
        ]);

        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $email = $request->email;
        $phone = $request->phone;
        $company = $request->company;
        $id = $request->id;

        employee::where('emp_id',$id)->update([
            'firstName' => $firstName,
            'lastName' => $lastName,
            'phone' => $phone,
            'email' => $email,
            'companyID' => $company,
        ]);

        return redirect('employeeList')->with('message','Success!');

    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required',
            'company' => 'required',

        ]);

        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $email = $request->email;
        $phone = $request->phone;
        $company = $request->company;

        $employee = new employee;
        $employee->firstName = $firstName;
        $employee->lastName = $lastName;
        $employee->email = $email;
        $employee->phone = $phone;
        $employee->companyID = $company;
        $employee->save();

        return redirect('createCompany')->with('message','Success!');

    }

}
