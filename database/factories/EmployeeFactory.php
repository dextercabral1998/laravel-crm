<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\employeeModel;
use Faker\Generator as Faker;

$factory->define(employeeModel::class, function (Faker $faker) {
    return [
        'firstName' =>  $faker->firstName,
        'lastName' =>  $faker->lastName,
        'email' => $faker->unique()->email,
        'phone' => $faker->number,
        'company' => $faker->firstName,
    ];
});
