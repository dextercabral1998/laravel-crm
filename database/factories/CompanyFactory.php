<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\companyModel;
use Faker\Generator as Faker;

$factory->define(companyModel::class, function (Faker $faker) {
    $company = $faker->unique()->company;
    return [

        'name' => $company,
        'email' => $faker->unique()->email,
        'website' =>  $company.".com",
        'logo' => $company.".jpg",
    ];
});


