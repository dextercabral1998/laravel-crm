<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use App\Models\companyModel;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class companyTest extends TestCase
{
    use RefreshDatabase;
    public function test_only_login_user_can_see_company_list()
    {
        $response = $this->get('/companyList')->assertRedirect('/login');
    }
    public function test_authenticated_user_can_see_company_list()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/companyList')->assertOk();
    }
    public function test_authenticated_user_can_create_company()
    {
        $faker = new Faker;
        $this->actingAs(factory(User::class)->create());

        $this->withOutExceptionHandling();
        $this->post('/saveCompany', [
            'name' => 'Da Company',
            'email' => 'dextercabral1998@gmail.com',
            'website' => 'website.com',
            'logo' =>  new \Illuminate\Http\UploadedFile(public_path('company_logo\1.jpg'), '1.jpg', null, null, null, true),

        ], ['X-Requested-With' => 'XMLHttpRequest']);
        $this->assertCount(1,companyModel::all());

    }
    public function test_authenticated_user_can_update_company()
    {
        $faker = new Faker;
        $this->actingAs(factory(User::class)->create());

        $this->withOutExceptionHandling();
        $this->post('/saveCompany', [
            'name' => 'Da Company',
            'email' => 'dextercabral1998@gmail.com',
            'website' => 'website.com',
            'logo' =>  new \Illuminate\Http\UploadedFile(public_path('company_logo\3.jpg'), '3.jpg', null, null, null, true),

        ], ['X-Requested-With' => 'XMLHttpRequest']);

        $company = companyModel::first();

        $response = $this->post('/updateCompany',[
            'name' => 'Dash Company',
            'email' => 'dextercabral1998@gmail.com',
            'website' => 'website.com',
            'id' => $company->company_id,
            'logo' =>  new \Illuminate\Http\UploadedFile(public_path('company_logo\4.jpg'), '4.jpg', null, null, null, true),

        ], ['X-Requested-With' => 'XMLHttpRequest']);


        $this->assertEquals('Dash Company',companyModel::first()->name);
    }
    public function test_authenticated_user_can_delete_company()
    {
        $faker = new Faker;
        $this->actingAs(factory(User::class)->create());

        $this->withOutExceptionHandling();
        $this->post('/saveCompany', [
            'name' => 'Da Company',
            'email' => 'dextercabral1998@gmail.com',
            'website' => 'website.com',
            'logo' =>  new \Illuminate\Http\UploadedFile(public_path('company_logo\5.jpg'), '5.jpg', null, null, null, true),

        ], ['X-Requested-With' => 'XMLHttpRequest']);
        $company = companyModel::first();



        $this->get('companyDelete/'.$company->company_id);
        $this->assertCount(0,companyModel::all());

    }

}
