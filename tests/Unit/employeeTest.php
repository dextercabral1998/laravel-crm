<?php

namespace Tests\Unit;


use Tests\TestCase;
use App\User;
use App\Models\employeeModel;
use App\Models\companyModel;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class employeeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    // use RefreshDatabase;
    public function test_only_login_user_can_see_employee_list()
    {
        $response = $this->get('/employeeList')->assertRedirect('/login');
    }
    public function test_authenticated_user_can_see_employee_list()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/employeeList')->assertOk();
    }



}
