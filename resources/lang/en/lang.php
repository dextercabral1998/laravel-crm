<?php
 return [
    'successAdded' => 'Data successfully added',
    'companyManagement'=> 'Company Management',
    'message'=> "Let's learn Laravel Localization",
    'update' => "Update",
    'create' => "Create",
    'delete' => "Delete",
    'viewList' => "View List",
    'phone' => "Phone",
    'email' => "Email",
    'action' => "Action",
    'company' => "Company",
    'firstName' => "First Name",
    'lastName' => "Last Name",
    'fullName' => "Full Name",
    'createEmployee' => "Create Employee",
    'empManagement' => "Employee Management",
    'empList' => "Employee List",
    'empTable' => "Employee Table",
    'empUpdate' => " Update Employee",
    'compUpdate' => "Company Update",
    'compName' => "Company Name",
    'compEmail' => "Company Email",
    'compWebsite' => "Company Website",
    'compLogo' => "Company Logo",
    'upload' => "Upload ",


    ]

?>
