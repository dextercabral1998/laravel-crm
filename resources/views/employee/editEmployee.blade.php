@extends('navigation.index')

@section('title', 'Update')
@section('page-title', __('lang.empUpdate') )

@section('body')
@parent

<div class="col-md-6">
    <!-- general form elements -->
<div class="card card-primary">
   <div class="card-header">
     <h3 class="card-title">{{ __('lang.empUpdate') }}</h3>
   </div>
   <!-- /.card-header -->
   <!-- form start -->
   <form method="POST" action="/updateEmployee" enctype="multipart/form-data">
    <div class="card-body">
      @if ($errors->any())
      <div class="alert alert-danger">
              <strong>Whoops!</strong> Please correct errors and try again!.
      <br/>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
      @if (session('message'))
          <div class="alert alert-success">Success saving data!</div>

      @endif
      @csrf
      <input type="hidden" name="id" value="{{ $employee->emp_id }}">
       <div class="form-group">
          <label for="firstName">{{ __('lang.firstName') }}</label>
          <input type="text" class="form-control" id="firstName" name="firstName"  value="{{ $employee->firstName }}" placeholder="First Name">
       </div>
       <div class="form-group">
           <label for="lastName">{{ __('lang.lastName') }}</label>
           <input type="text" class="form-control" id="lastName" name="lastName" value="{{ $employee->lastName }}" placeholder="Last Name">
        </div>
        <div class="form-group">
           <label for="email">{{ __('lang.email') }}</label>
           <input type="email" class="form-control" id="email" name="email" value="{{ $employee->email }}" placeholder="Email">
        </div>
        <div class="form-group">
           <label for="phone">{{ __('lang.phone') }}</label>
           <input type="text" class="form-control" id="phone" name="phone" value="{{ $employee->phone }}" placeholder="Phone">
        </div>
        <div class="form-group">
           <label for="phone">{{ __('lang.company') }}</label>
           <select class="form-control" id="company" name="company">
               <option value="" hidden>Select Company</option>
               @foreach ($company as $company)
                    @if ($company->company_id == $employee->companyID)
                        <option selected value="{{ $company->company_id }}">{{ $company->name }}</option>
                    @else
                        <option value="{{ $company->company_id }}">{{ $company->name }}</option>
                    @endif

               @endforeach

           </select>
        </div>




    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">{{ __('lang.update') }}</button>
    </div>
  </form>
 </div>
 <!-- /.card -->
</div>

@endsection

@section('scripts')
@parent
    <script>
        $('#employee').addClass('active');
   </script>
@endsection



