@extends('navigation.index')

@section('title', __('lang.empList'))
@section('page-title', __('lang.empList'))

@section('body')
@parent

    <div class="col-md-12">
        @if (session('message'))
        <div class="alert alert-success">Success updating data!</div>

    @endif
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{ __('lang.empTable') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div>

                <table class="table table-striped">
                    <thead>
                      <tr>

                        <th>{{ __('lang.fullName') }}</th>
                        <th>{{ __('lang.email') }}</th>
                        <th>{{ __('lang.phone') }}</th>
                        <th>{{ __('lang.company') }}</th>
                        <th>{{ __('lang.action') }}</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                            <tr>

                                <td>{{ $employee->firstName."  ".$employee->lastName }}</td>
                                <td>{{ $employee->email }}</td>
                                <td>{{ $employee->phone }}</td>
                                <td>{{ $employee->company_name }}</td>
                                <td>
                                    <a class="btn btn-primary" href="employeeEdit/{{ $employee->id }}">Update</a>
                                    <button class="btn btn-danger" onclick="deletedata({{  $employee->id }},'{{ $employee->firstName }}');">Delete</button>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>

              </div>
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <div class="col-1 offset-11">
            {{ $employees->links() }}
          </div>
    </div>

@endsection

@section('scripts')
@parent

    <script>
        function deletedata(id,name)
        {
            if (confirm('Delete '+name+'?')) {
            // delete it!
            $.ajax({

                    url: 'employeeDelete/'+id,
                    type: "GET",
                success:function(data){

                    alert("Successfully Deleted!");
                    location.reload();
                },
                error:function (){
                    alert("Something went wrong!");
                }
            });
            }
             else {
            // Do nothing!

            }
        }

         $('#employeeList').addClass('active');
    </script>
@endsection



