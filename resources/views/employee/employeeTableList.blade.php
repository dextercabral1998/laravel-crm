@extends('navigation.index')

@section('title', __('lang.empList'))
@section('page-title', __('lang.empList'))

@section('body')
@parent

<div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ __('lang.empTable') }}</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="employeeTable" class="table table-bordered table-hover">
          <thead>
            <tr>

                <th>{{ __('lang.fullName') }}</th>
                <th>{{ __('lang.email') }}</th>
                <th>{{ __('lang.phone') }}</th>
                <th>{{ __('lang.company') }}</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($employees as $employee)
            <tr>

                <td>{{ $employee->firstName."  ".$employee->lastName }}</td>
                <td>{{ $employee->email }}</td>
                <td>{{ $employee->phone }}</td>
                <td>{{ $employee->company_name }}</td>

            </tr>
        @endforeach
          </tbody>

        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
@endsection

@section('scripts')
@parent

    <script>
$('#employeeTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    </script>
@endsection



