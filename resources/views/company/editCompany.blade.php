@extends('navigation.index')

@section('title', 'Update')
@section('page-title', __('lang.compUpdate'))

@section('body')
@parent

<div class="col-md-6">
    <!-- general form elements -->
<div class="card card-primary">
   <div class="card-header">
     <h3 class="card-title">{{ __('lang.compUpdate') }}</h3>
   </div>
   <!-- /.card-header -->
   <!-- form start -->
   <form method="POST" action="/updateCompany" enctype="multipart/form-data">
     <div class="card-body">
       @if ($errors->any())
       <div class="alert alert-danger">
               <strong>Whoops!</strong> Please correct errors and try again!.
       <br/>
               <ul>
                   @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                   @endforeach
               </ul>
           </div>
       @endif

       @csrf
       <input type="hidden" name="id" value="{{ $company->company_id }}">
       <div class="form-group">
           <label for="name">{{ __('lang.compName') }}</label>
           <input type="text" class="form-control" id="name" name="name"  value="{{ $company->name }}" placeholder="Company Name">
         </div>
       <div class="form-group">
         <label for="email">{{ __('lang.compEmail') }}</label>
         <input type="email" class="form-control" id="email" name="email"  value="{{ $company->email }}" placeholder="Company Email">
       </div>
       <div class="form-group">
           <label for="website">{{ __('lang.compWebsite') }}</label>
           <input type="text" class="form-control" id="website" name="website"  value="{{ $company->website }}" placeholder="Company Website">
         </div>
         <div class="form-group">
           <label for="exampleInputFile">{{ __('lang.compLogo') }}</label>
           <div class="input-group">
             <div class="custom-file">
               <input type="file" class="custom-file-input" name="logo"  value="{{ $company->logo }}" id="exampleInputFile">
               <label class="custom-file-label" for="exampleInputFile">Choose file</label>
             </div>
             <div class="input-group-append">
               <span class="input-group-text">{{ __('lang.upload') }}</span>
             </div>
           </div>
         </div>
         <input type="hidden" name="logoHolder" value="{{ $company->logo }}" >
         {{-- <input type="file" name="logo" value="company_logo/{{ $company->logo }}" > --}}
     </div>
     <!-- /.card-body -->

     <div class="card-footer">
       <button type="submit" class="btn btn-primary">{{ __('lang.update') }}</button>
     </div>
   </form>
 </div>
 <!-- /.card -->
</div>

@endsection

@section('scripts')
@parent
    <script>
        $('#company').addClass('active');
   </script>
@endsection



