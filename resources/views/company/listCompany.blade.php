@extends('navigation.index')

@section('title', 'Company Lists')
@section('page-title', 'Company Lists')

@section('body')
@parent

    <div class="col-md-12">
        @if (session('message'))
        <div class="alert alert-success">Success updating data!</div>

    @endif
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Company Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div>

                <table class="table table-striped">
                    <thead>
                      <tr>

                        <th>{{ __('lang.compName') }}</th>
                        <th>{{ __('lang.compEmail') }}</th>
                        <th>{{ __('lang.compLogo') }}</th>
                        <th>{{ __('lang.compWebsite') }}</th>
                        <th>{{ __('lang.action') }}</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($companies as $company)
                            <tr>

                                <td>{{ $company->name }}</td>
                                <td>{{ $company->email }}</td>
                                <td><img src="company_logo/{{ $company->logo }}" style="height:35px"/>  </td>
                                <td>{{ $company->website }}</td>
                                <td>
                                    <a class="btn btn-default" href="viewEmployee/{{ $company->company_id }}">{{ __('lang.viewList') }}</a>
                                    <a class="btn btn-primary" href="companyEdit/{{ $company->company_id }}">{{ __('lang.update') }}</a>
                                    <button class="btn btn-danger" onclick="deletedata({{  $company->company_id }},'{{ $company->name }}');">{{ __('lang.delete') }}</button>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>

              </div>
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <div class="col-1 offset-11">
            {{ $companies->links() }}
          </div>
    </div>

@endsection

@section('scripts')
@parent

    <script>
        function deletedata(id,name)
        {
            if (confirm('Delete '+name+'?')) {
            // delete it!
            $.ajax({

                    url: 'companyDelete/'+id,
                    type: "GET",
                success:function(data){

                    alert("Successfully Deleted!");
                    location.reload();
                },
                error:function (){
                    alert("Something went wrong!");
                }
            });
            }
             else {
            // Do nothing!

            }
        }

         $('#companylist').addClass('active');
    </script>
@endsection



